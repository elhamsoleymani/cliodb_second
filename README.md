# ClioDB Second Project

## Repository structure
### /Code/
In this folder can be found all the script used for the creation of the Database in neo4j. Other than the script present in the folder that are used throughout the creation, the folder /utils/contains some of the scripts used during the development of the project, for debugging or data exploration purposes.
### /Datasets/
This folder contains every dataset needed for the creation of the database. After cloning the repository this folder should contain 3 files: 
- awards.csv: file containing the informations about the awards that can be found in Tripadvisor.com and Wikipedia.org;
- cost\_restaurant\_cities.csv: Average cost about medium range restaurants in a sample of european cities, provided by numbeo.com;
- cuisines\_ratings.csv: A ranking of the best cuisines in the world provided by tasteatlas.com with the descriptions from wikipedia.org.
After the "Step 1" and "Step 2" described in the setup sections the folder should contain also the files:
- dishes.csv: list of dishes provided by Yummly28k through lherranz.org;
- tripadvisor\_european\_restaurants\_clean.csv: list of european restaurants with various attributes provided by tripadvisor.com through kaggle.com.

### /Graph/
Contains the Graph produced thanks to Arrows.app in its JSON and PNG versions.

## Setup

### Prerequisites
Python 3 has to be installed in the system along side Neo4j and an interpret for Jupyter Notebooks 
### Step 1: Downloading the datasets
1) The main csv files has to be downloaded from "https://www.kaggle.com/stefanoleone992/tripadvisor-european-restaurants" and saved in the folder /Datasets/ with the name "tripadvisor\_european\_restaurants\_clean.csv";
2) The dataset about the dishes has to be downloaded from "https://www.lherranz.org/local/datasets/yummly28k/Yummly28K.rar". From the extracted folder, only the "metadata27638" folder is needed. It's content has to be put in a folder inside Dataset called /Dishes/.
### Step 2: Polishing the datasets
1) Run the mainDataset\_polish.py script to clean the main dataset of the restaurants to correct some errors in the csv standard and do other minor changes to facilitate the data ingestion in the DB;
2) Run the dishes\_json2csv.py script to transform the JSON files present in the Datasets/Dishes/ folder in a single dishes.csv file
### Step 3: Basic extractions with Cypher
1) Create a neo4j database called "RDB" with password "RDB";
2) Fix the settings of the DB for permitting access and the loading of external csv files;
3) Change the paths inside the pre\_ingestion.cql to represent the real absolute position of the csv files;
4) Run inside the DB in neo4j the pre\_ingestion.cql file to do the initial extractions.
### Step 4: Main extraction with Jupyter Notebook
With the neo4j DB open, run the Jupyter Notebook called "dataIngestion.ipynb". The code is commented to explain every step of the extractions. 

The code has been testes with various PCs with varying availability of resources. The main bottleneck is in the parsing by pandas of the main csv file. A way to fix problems that can arise in that situation is adapting the parsing using the functions provided by pandas of random sampling of the csv, or decreasing the number of rows analyzed by the script. Anyway, the defaults should be enough. 
### Step 5: Final extractions
1) Change the paths inside the post\_ingestion.cql to represent the real absolute position of the csv files;
2) Run inside the DB in neo4j the post\_ingestion.cql file to do the final extractions.

### Step 6: Queries
The ten queries are present in a file inside the /Code/ folder called "queries.cql".
