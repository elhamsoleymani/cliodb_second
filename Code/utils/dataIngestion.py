# required libraries
from datetime import date
import pandas as pd
import os
from pathlib import Path
import ast
import re

def cleaner(str):
    str=str.replace('\\""', '')
    str=str.replace('€', '')
    return str

# Neo4J params class
class Neo4jParams:
  def __init__(self, user, psw,dbname,db_psw,uri):
    self.user = user
    self.psw = psw
    self.dbname = dbname
    self.dbpsw = dbpsw
    self.uri = uri

#DB parameters
user="neo4j"
#psw="password"
psw="neo4j"
dbname="RDB"
dbpsw="RDB"
uri = "bolt://localhost:7687"

params = Neo4jParams(user,psw,dbname,dbpsw,uri)

from neo4j import GraphDatabase, data

# test class
class Driver:

    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        self.driver.close()

# parameters and URLs
path = str(Path(os.path.abspath(os.getcwd())).parent.absolute())
main_data = path + '/Datasets/tripadvisor_european_restaurants.csv'

# Load the CSV files in memory
restaurants = pd.read_csv(main_data, sep=',', keep_default_na=False, na_values=['_'])
# less_restaurants = restaurants.sample(n=100000)
# less_restaurants.info()
restaurants = restaurants.sort_values(['reviews_count_in_default_language'], ascending=False) # da checkare
# restaurants.info()

# connect to the DB
driver = GraphDatabase.driver(params.uri, auth=(params.dbname, params.dbpsw))
# create a session
session = driver.session()
counter=0
for index, row in restaurants.iterrows():
    # Managment of the loop
    if counter%10==0: print(counter)
    if counter==500: break
    counter+=1

    # Extraction of interesting fields
    restaurant_link = row['restaurant_link']
    restaurant_name = row['restaurant_name'] 
    restaurant_popularity = row['popularity_generic']    
    restaurant_rating = row['avg_rating']
    restaurant_n_reviews = row['total_reviews_count'] 
    restaurant_days = row['open_days_per_week'] 
    restaurant_hours = row['open_hours_per_week']
    location_region = row['region']
    location_province = row['province'] 
    location_city = row['city']
    location_address = row['address'] 
    location_country = row['country']
    price_range = cleaner(row['price_range'])
    language = row['default_language']
    awards = row['awards']
    cuisine = row['cuisines']
    location_fullAddress = location_country+", "+location_region+", "+location_province+", "+location_city+", "+location_address 
    
    # Creation of Location
    session.run("MERGE (l:Location{fullAddress: $_location_fullAddress}) SET l+={region: $_location_region, province: $_location_province, city: $_location_city, address: $_location_address}",
    _location_fullAddress=location_fullAddress,
    _location_address=location_address,
    _location_city=location_city,
    _location_province=location_province,
    _location_region=location_region,
    )

    # Creation of Restaurant
    session.run("MERGE (r:Restaurant{link: $_restaurant_link}) SET r += {name: $_restaurant_name,rating: $_restaurant_rating, ranking: $_restaurant_popularity, n_reviews: $_restaurant_n_reviews, days: $_restaurant_days, hours: $_restaurant_hours}",
    _restaurant_link = restaurant_link,
    _restaurant_name = restaurant_name,
    _restaurant_popularity = restaurant_popularity,
    _restaurant_rating = restaurant_rating,
    _restaurant_n_reviews = restaurant_n_reviews,
    _restaurant_days = restaurant_days,
    _restaurant_hours = restaurant_hours)

    ## Creation of Price

    avg=0
    if price_range.find('-')!=-1:
        tmp=price_range.split('-')
        numeric_filter1 = filter(str.isdigit,tmp[0])
        numeric_filter2 = filter(str.isdigit,tmp[1])
        numeric_string1 = "".join(numeric_filter1)
        numeric_string2 = "".join(numeric_filter2)
        lower = int(numeric_string1)
        higher = int(numeric_string2)
        avg = int((lower+higher)/2)
        price_range = numeric_string1+'-'+numeric_string2
        session.run("MERGE (p:Price{range: $_price_range, average: $_avg})",
        _price_range= price_range,
        _avg=avg)
        ## hasPriceRange
        session.run("MATCH (r:Restaurant{link: $_restaurant_link}), (p:Price{range: $_price_range, average: $_avg}) MERGE (r)-[:hasPriceRange]->(p)",
        _restaurant_link = restaurant_link,
        _price_range=price_range,
        _avg=avg)

    ## speaks
    session.run("MATCH (lan:Language{name: $_language}), (r:Restaurant{link: $_restaurant_link}) MERGE (r)-[:speaks]->(lan)",
    _language = language,
    _restaurant_link = restaurant_link
    )

    ## isLocated
    session.run("MATCH (l:Location{fullAddress: $_location_fullAddress}), (r:Restaurant{link: $_restaurant_link}) MERGE (r)-[:isLocated]->(l)",
    _restaurant_link = restaurant_link,
    _location_fullAddress = location_fullAddress
    )

    ## hasCountry
    session.run("MATCH (l:Location{fullAddress: $_location_fullAddress}), (co:Country) WHERE co.name CONTAINS $_location_country MERGE (l)-[:hasCountry]->(co)",
    _location_fullAddress = location_fullAddress,
    _location_country = location_country
    )

    ## hasPriceRange
    session.run("MATCH (r:Restaurant{link: $_restaurant_link}), (p:Price{range: $_price_range, average: $_avg}) MERGE (r)-[:hasPriceRange]->(p)",
    _restaurant_link = restaurant_link,
    _price_range=price_range,
    _avg=avg
    )

    ## wonAward 
    awards_years_list = re.findall(r'\d+', awards)
    awards_names_list = awards.split(',')
    new_names = []
    for x in awards_names_list:
        tmp_string = ''.join([i for i in x if not i.isdigit()]).strip()
        if tmp_string.replace('  ','')!="":
            new_names.append(tmp_string.replace('  ',''))
    if len(new_names)!=0: 
        while len(new_names)>len(awards_years_list):
            awards_years_list.append('0')
        for index, aw in enumerate(new_names):
            session.run("MATCH (r:Restaurant{link: $_restaurant_link}), (a:Award) WHERE a.name CONTAINS $_aw MERGE (r)-[:wonAward{date: $_date}]->(a)",
            _restaurant_link = restaurant_link,
            _aw = aw,
            _date = awards_years_list[index]
            )

    # servesCuisine
    cuisines_names = cuisine.split(',')
    new_names_cus = []
    for x in cuisines_names:
        new_names_cus.append(x.replace('  ','').strip())
    if len(new_names_cus)!=0:
        for cus in new_names_cus:
            if cus!="":
                session.run("MATCH (r:Restaurant{link: $_restaurant_link}), (c:Cuisine) WHERE c.name CONTAINS $_cus MERGE (r)-[:servesCuisine]->(c)",
                _restaurant_link = restaurant_link,
                _cus = cus
                )


Dish_file = path + '/Datasets/dishes.csv'
dishes_file = pd.read_csv(Dish_file, sep=',', keep_default_na=False, na_values=['_'])
for index, row in dishes_file.iterrows():
    if index%10==0: print(index)
    if index==500: break
    dish_cuisine=ast.literal_eval(row["cuisine"])
    dish_name=row["name"]
    for cuisines in dish_cuisine:
        # hasCuisine
        session.run("MATCH (c:Cuisine{name: $_dish_cuisine}), (d:Dish{name: $_dish_name}) MERGE (d)-[:hasCuisine]->(c)",
        _dish_name=dish_name,
        _dish_cuisine=cuisines)
        # servesDish
        session.run("MATCH (r:Restaurant), (c:Cuisine{name: $_dish_cuisine}), (d:Dish{name: $_dish_name}) WHERE r.name STARTS WITH left($_dish_name,1) MERGE (r)-[:servesDish]->(d)",
        _dish_name=dish_name,
        _dish_cuisine=cuisines
        )
    
