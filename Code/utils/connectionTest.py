# required libraries
import pandas as pd
import os
from pathlib import Path

# Neo4J params class
class Neo4jParams:
  def __init__(self, user, psw,dbname,db_psw,uri):
    self.user = user
    self.psw = psw
    self.dbname = dbname
    self.dbpsw = dbpsw
    self.uri = uri


#DB parameters
user="neo4j"
#psw="password"
psw="neo4j"
dbname="RDB"
dbpsw="RDB"
uri = "bolt://localhost:7687"

params = Neo4jParams(user,psw,dbname,dbpsw,uri)

from neo4j import GraphDatabase

# test class

class Driver:

    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        self.driver.close()

    def print_greeting(self, message):
        with self.driver.session() as session:
            greeting = session.write_transaction(self._create_and_return_greeting, message)
            print(greeting)

    @staticmethod
    def _create_and_return_greeting(tx, message):
        result = tx.run("CREATE (a:Greeting) "
                        "SET a.message = $message "
                        "RETURN a.message + ', from node ' + id(a)", message=message)
        return result.single()[0]


if __name__ == "__main__":
    greeter = Driver("bolt://localhost:7687", "RDB", "RDB")
    greeter.print_greeting("hello, world")
    greeter.close()
"""
import pprint

driver = GraphDatabase.driver(params.uri, auth=(params.dbname, params.dbpsw))
session = driver.session()
battleType = "ambush"
result = session.run("MATCH (b:Battle {type: $type}) RETURN b.name AS name", type=battleType)
names = [record["name"] for record in result]
pp = pprint.PrettyPrinter(indent=4)
pp.pprint(names)
session.close()


driver.close()
"""