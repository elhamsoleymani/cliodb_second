import os
import csv
import json
"""
file = open('../Datasets/Dishes/meta00001.json')
data=json.load(file)
del data['ingredientLines']
del data['attribution']
del data['yield']
del data['nutritionEstimates']
del data['source']
del data['flavors']
del data['images']
del data['numberOfServings']
with open('data.txt', 'w') as outfile:
    json.dump(data, outfile)
"""
fcsv = csv.writer(open("../Datasets/dishes.csv", "w", newline=''))
fcsv.writerow(["totalTime", "name", "rating", "course", "cuisine"])
for filename in os.listdir("../Datasets/Dishes/"):
    with open(os.path.join("../Datasets/Dishes/", filename), 'r') as f:
        data=json.load(f)
        fcsv.writerow([data['totalTime'],
            data['name'],
            data['rating'],
            data['attributes']['course'],
            data['attributes']['cuisine']]
        )
